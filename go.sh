#!/bin/sh
pkg update && pkg upgrade -y && pkg install -y fish
### Copy fish functions ####
git clone https://gitlab.com/HugoDaniel/dragonkiss/
echo "Copying fish functions"
cp -R dragonkiss/functions/* /usr/local/share/fish/vendor_functions.d
### Init fish scripts ###
chmod +x /root/dragonkiss/init.fish
/usr/local/bin/fish dragonkiss/init.fish $1 $2
