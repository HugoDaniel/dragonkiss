if test (count $argv) = 0
  echo "Username to create for SSH login:"
  read username
else 
  set username $argv[1]
end
if test (count $argv) = 2
  set ghusername $argv[2]
else
  echo "GitHub username to get the SSH keys:"
  read ghusername
end
echo "Creating user $username"
pw useradd $username -m -s /usr/local/bin/fish -G wheel
and mkdir /home/$username/.ssh
and curl https://github.com/$ghusername.keys > /home/$username/.ssh/authorized_keys
echo "User $username can now login with SSH"
dragon init
