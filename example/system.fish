set -l name (basename (status -f) .fish){_uninstall}

echo "NAME $name"
function system_create
# read filesystem
# read users
# read firewall
# read pkgs 
# read jails
# read services
# copy files
end

function system_revert
# this is always called before system_create
# it must allow multiple calls of system_create to be idempotent
end

function system_verify
# verify that your system is up and running correctly
end

