High level service configuration

Snapshots
/ @ROOT
/etc @ETC
/usr/local/etc @LOCALETC
/home @HOME
/usr/local/jails/

Problemas:
- ter um nginx no host a servir servers e locations das jails
/services/http/localhost/server_name "localhost"
/services/http/localhost/root "$jail1_root/home/someone/content"
/services/http/localhost/log  "$jail1_root/home/someone/log"
/services/http/localhost/default "notls"\n"wordpress"
or
/services/http/localhost/server.conf


- ter um nginx a servir um wordpress que está na home de um user
/services/http/mysite.pt/server_name "mysite.pt www.mysite.pt"
/services/http/mysite.pt/root "/home/someuser/wordpress"
/services/http/mysite.pt/log "/home/someuser/logs"
/services/http/mysite.pt/default "wordpress"

- ter um nginx a servir um gitea na home de um user numa jail
/services/http/git.mysite.pt/server_name "git.mysite.pt"
/services/http/git.mysite.pt/root "$jail2_root/home/someuser/gitea"
/services/http/git.mysite.pt/log "$jail2_root/home/someuser/log"
/services/http/git.mysite.pt/default "static_files"
/services/http/git.mysite.pt/default_extension "location / { proxy_pass http://jail1:8000; }"

- ter um nginx a servir um proj pessoal node que depende de postgresql na home de um user
