# dragonkiss

DragonflyBSD fast initialization scripts

After installation, login as root and run:

`curl https://gitlab.com/HugoDaniel/dragonkiss/-/raw/master/go.sh > go.sh && sh go.sh`

It will:
- Ask the for username to create with SSH access
- Ask for the GitHub account to fetch the SSH keys
- Update the pkg db
- Install fish shell

Alternatively if you don't want to have to type the username and GitHub account, pass them as arguments:

`curl https://gitlab.com/HugoDaniel/dragonkiss/-/raw/master/go.sh > go.sh && sh go.sh username githubUser`
