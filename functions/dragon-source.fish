function dragon-source
switch $argv[1]
  case init
    if test ! -d /usr/src 
      cd /usr
      make src-create
      cd /usr/src
      set VERSION (uname -r | cut -f 1 -d '-' | tr '.' '_')
      git checkout DragonFly_RELEASE_$VERSION
    end
  case build
    cd /usr/src
    set CPUS (sysctl hw.ncpu | cut -c "10-")
    set BUILDCPUS (echo $CPUS" + 1" | bc)
    and make -j $BUILDCPUS buildworld
    and make -j $BUILDCPUS buildkernel
  case '*'
    echo Available subcommands:
    echo - init (Installs source code)
    echo - build (make buildworld && make buildkernel)
end
end
