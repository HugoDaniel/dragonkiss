### Generates a 16 char random passsword
function pass
  head -c100 /dev/urandom | strings -n1 | tr -d '[:space:]' | head -c16
end
