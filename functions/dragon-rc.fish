function dragon-rc
  function dragon-rc-new-content
    set usage "Usage: dragon-rc-new-content config-file init-pattern-file content-string"
    if test ! (count $argv) = 3
      echo $usage
    end
    if test ! -e $argv[1]
      echo "Config not found '$argv[1]'."\n"$usage"
      return 1
    end
    if test ! -e "dragonkiss/templates/$argv[2]"
      echo "Init pattern file not found '$argv[2]'."\n"$usage"
      return 1
    end
    set endPattern (cat dragonkiss/templates/end.rc.conf)
    set startPattern (cat "dragonkiss/templates/"$argv[2])
    set content (sed "/$startPattern/,/$endPattern/!d" $argv[1])
    if test "$content" = "" 
      set content $startPattern $endPattern
    end
    set content $content[1..-2] $argv[3] (cat dragonkiss/templates/end.rc.conf)
    string split \n $content
  end
  function dragon-rc-without-content
    set usage "Usage: dragon-rc-without-content config-file init-pattern-file"
    if test ! (count $argv) = 2
      echo $usage
    end
    if test ! -e $argv[1]
      echo "Config not found '$argv[1]'."\n"$usage"
      return 1
    end
    if test ! -e "dragonkiss/templates/$argv[2]"
      echo "Init pattern file not found '$argv[2]'."\n"$usage"
      return 1
    end
    set endPattern (cat dragonkiss/templates/end.rc.conf)
    set startPattern (cat "dragonkiss/templates/"$argv[2])
    sed "/$startPattern/,/$endPattern/d" $argv[1]
  end
  set usage "Usage: dragon-rc config-file init-pattern-file content-string"
  set newContent (dragon-rc-new-content $argv)
  printf '%s\n' (dragon-rc-without-content $argv[1..2]) > $argv[1]
  printf '%s\n' $newContent >> $argv[1]
end
