function dragon-jail
  function dragon-jail-rc
    dragon-rc "/etc/rc.conf" "jail_init.rc.conf" $argv
  end
  function dragon-jail-init
    # setup rc.conf
    dragon-jail-rc 'jail_enable="YES"'
    dragon-jail-rc 'jail_list="'(string join " " $argv)'"'
    # set socket overload
    sysctl jail.defaults.net_raw_sockets=1
    sysctl jail.defaults.allow_listen_override=1
  end
  function dragon-jail-create
    if test ! (count $argv) = 3
      echo "usage: dragon-jail-create jailname jailhostname jailip
      return 1
    end
    set rootdir "/usr/jail/"$argv[1]
    dragon-jail-rc 'jail_'$argv[1]'_rootdir="$rootdir"'
    dragon-jail-rc 'jail_'$argv[1]'_hostname="$argv[2]"'
    dragon-jail-rc 'jail_'$argv[1]'_ip="$argv[3]"'
    mkdir -p $rootdir
    cd /usr/src
    make installworld DESTDIR=$rootdir
    cd etc
    make distribution DESTDIR=$rootdir -DNO_MAKEDEV_RUN
    cd $rootdir
    ln -sf dev/null kernel
    mount_devfs -o jail $rootdir/dev
  end

  dragon-jail-init $argv
  for i in (seq (count $argv))
    set jailhost "dragon"$argv[$i]
    if test -n "$$argv[$i]"
      set jailhost $$argv[$i]
    end
    dragon-jail-create $argv[$i] $jailhost 192.168.100.2$i
  end
end
