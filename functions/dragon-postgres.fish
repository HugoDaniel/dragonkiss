function dragon-postgres
switch $argv[1]
  case init
    pkg install -y postgresql12-server postgresql12-client postgresql12-contrib
  case '*'
    echo Available subcommands:
    echo - init (Installs postgresql)
end
end
