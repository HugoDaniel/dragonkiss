####
# DragonKISS 
#
# 1. dragonkiss init [path]
# 2. dragonkiss switch [path | number | relative number]
# 3. dragonkiss list
####
function dragonkiss -a operation path
  switch $operation
    case use 
      echo "USE"
    case list
      echo "LISTING"
    case '*'
      echo "usage: dragonkiss [use|list] [path|number|relative number]"
  end
end

function dragonsystem -a path

end
