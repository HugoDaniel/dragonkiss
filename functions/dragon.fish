function dragon
switch $argv[1]
case init
  echo Running initialization script
  dragon-init
case '*'
  echo Available subcommands:
  echo - init (runs the system initialization scripts)
end
end
