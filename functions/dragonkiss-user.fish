function dragonkiss-user -a path
  _dragonuser_read $path; or return $status
  _dragonuser_create
  echo 'All good'
end

function _dragonuser_create
## Check if user exists
  set result (pw usershow $username -q)
  if test $status -eq 0
    ## Exists
    pw useradd $username -n $username -m -c $userfullname -N -q
    echo 'Updating ' $username $userfullname
  else
    ## Does not exist, create it
    pw useradd $username -n $username -m -c $userfullname -N -q
    echo 'Creating ' $username $userfullname
  end
end

function _dragonuser_exists -a user
  set result (pw usershow $user)
  return test $status -eq 0 
end

function _dragonuser_read -a path
  ## validate path
  ## path must be a directory
  if ! test -d $path
    echo "Invalid path for user " $path >&2
    return 1
  end
  set -g username (basename $path)
  ## username must be composed of only ascii characters
  if test (string match -r '[a-zA-Z][a-zA-Z0-9]*' $username) != $username
    echo "User '"$username"' must be composed of valid characters ([a-zA-Z][a-zA-Z0-9]*)" >&2
    return 1
  end
  ## path must have a 'name' file
  if ! test -f $path/name
    echo "User "$username" must have a name at "$path"/name" >&2
    return 1
  end
  ## 'name' file must not be empty
  set -g userfullname (cat $path/name)
  if test -z $userfullname
    echo $path"/name must not be empty" >&2
    return 1
  end
  return 0
end

function dragonuser_read_test
  printf "######## dragonuser_read\n" 
  printf "#\t should report an error if the path is invalid..." 
  _dragonuser_read ./tests/dragonkiss-user/somethingthatdoesnotexist 2> /dev/null
  test $status -eq 0; and printf "FAIL" && return 1; or printf "OK\n"

  printf "#\t should report an error if no name is present..." 
  _dragonuser_read ./tests/dragonkiss-user/user1 2> /dev/null
  test $status -eq 0; and printf "FAIL" && return 1; or printf "OK\n"

  printf "#\t should report an error if username is not valid..." 
  _dragonuser_read ./tests/dragonkiss-user/_invalid1 2> /dev/null
  test $status -eq 0; and printf "FAIL" && return 1; or printf "|OK|"
  _dragonuser_read ./tests/dragonkiss-user/123invalid 2> /dev/null
  test $status -eq 0; and printf "FAIL" && return 1; or printf "OK\n"

  printf "#\t should report an error if name is empty..." 
  _dragonuser_read ./tests/dragonkiss-user/emptyname 2> /dev/null
  test $status -eq 0; and printf "FAIL" && return 1; or printf "OK\n"

end

